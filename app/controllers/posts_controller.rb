class PostsController < ApplicationController
  skip_before_action :authorize_request, only: :index

  def index
    @posts = Post.all
    json_response(@posts)    
  end

  def create
    @post = Post.create!(content: post_params[:content], user_id: @current_user.id)
    json_response(@post, :created)
  end

  private

  def post_params
    params.permit(:content, :user_id)
  end

end
