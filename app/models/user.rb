class User < ApplicationRecord
  has_secure_password

  has_many :posts, dependent: :destroy

  validates_presence_of :name, :email, :password_digest
  validates_uniqueness_of :email
  validates_length_of :password_digest, minimum: 8
end
