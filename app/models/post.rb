class Post < ApplicationRecord
  belongs_to :user
  serialize :user, JSON
  alias_attribute :author, :user

  validates_presence_of :content
  validates_length_of :content, maximum: 120
end
