class AuthorPreviewSerializer < ActiveModel::Serializer
  attributes :id, :name, :email
end
