FactoryBot.define do
  factory :post do
    content { Faker::Lorem.paragraph_by_chars(number: 120, supplemental: false) }
    user_id { Faker::Number.number(10) }
  end
end