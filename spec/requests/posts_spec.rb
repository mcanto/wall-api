require 'rails_helper'

RSpec.describe 'Posts API', type: :request do

  let(:user) { create(:user) }
  let!(:posts) { create_list(:post, 10, user_id: user.id) }
  let(:headers) { valid_headers }

  describe 'GET /posts' do
    context 'when valid request' do

      before { get '/posts' }

      it 'returns all posts' do
        expect(json).not_to be_empty
        expect(json.size).to eq(10)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'POST /posts' do
    let(:valid_attributes) { { content: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum"} }
    context 'when valid request' do
      before {post '/posts', params: valid_attributes.to_json, headers: headers  }

      it 'creates a post' do
        expect(json['content']).to eq('Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum')
        expect(json['user_id']).to eq(user.id)
        expect(response).to have_http_status(201)
      end
    end

    context 'when request is invalid' do

      let(:invalid_attributes) { { content: nil }.to_json }

      before {post '/posts', params: invalid_attributes.to_json, headers: valid_headers}
      it "doesn't create post" do
        expect(response).to have_http_status(422)
      end
      it 'returns a error message' do
        expect(json['message']).to match(/Validation failed: Content can't be blank/)
      end
    end

  end
end
