require "rails_helper"

RSpec.describe "User Notifier Mailer", type: :mailer do
  before(:each) do
    ActionMailer::Base.delivery_method = :test
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  after(:each) do
    ActionMailer::Base.deliveries.clear
  end

  describe "sending welcome email" do
    let(:user) { create(:user) }

    it 'sent welcome mail' do
      UserNotifierMailer.welcome(user).deliver_now
      expect(ActionMailer::Base.deliveries.count).to eq(1)
    end

    it 'sent welcome mail to the right user' do

      UserNotifierMailer.welcome(user).deliver_now

      mail = ActionMailer::Base.deliveries.last.to.first
      expect(mail).to eq(user.email)
    end
  end
end
